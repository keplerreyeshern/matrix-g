<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Gallery;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str as Str;

class CategoryController extends Controller
{

    protected $pages;
    protected $title;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->title = "Categorias";
        $this->pages = 'category';

        View::share('title', $this->title);
        View::share('pages', $this->pages);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::whereNull('parent_id')->get();
        return view('admin.categories.list', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  int  $parent_id
     * @return \Illuminate\Http\Response
     */
    public function createChild($parent_id)
    {
        $category = Category::find($parent_id);
        return view('admin.categories.create', compact('parent_id', 'category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string|min:3|max:250'
        ],[
            'name.required' => 'El campo nombre es obligatorio',
            'name.string' => 'El campo nombre debe ser una cadena de texto',
            'name.min' => 'El campo nombre debe tener minimo 3 caracteres',
            'name.max' => 'El campo nombre debe tener maximo 250 caracteres'
        ]);
        $category = new Category();
        $category->name = $data['name'];
        $category->slug = Str::slug($data['name']);
        $category->description = $request['description'];
        $category->parent_id = $request['parent_id'];
        $category->active = true;
        $image = $request->file('image');
        if($image) {
            $nameImage = time() . "_" . $image->getClientOriginalName();
            Storage::disk('public')->put('/images/categories/' . $nameImage, \File::get($image));
            $category->image = '/storage/images/categories/'.$nameImage;
        }
        $category->save();
        $galleries = Gallery::whereNull('reference')->where('type', 'categories')->get();
        if($galleries){
            for($i=0;count($galleries)>$i;$i++){
                $galleries[$i]->reference = $category->id;
                $galleries[$i]->save();
            }
        }
        return redirect(route('categories.index'))->with('success', '¡La categoria se creo con exito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categories = Category::    where('parent_id', $id)->get();
        return $categories;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        $gallery =  Gallery::where('active', true)->where('reference', $category->id)->where('type', 'categories')->first();
        $images = [];
        if ($gallery){
            $images = Image::where('gallery_id', $gallery->id)->get();
        }
        return view('admin/categories/edit', compact('category', 'images'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'name' => 'required|string|min:3|max:250'
        ],[
            'name.required' => 'El campo nombre es obligatorio',
            'name.string' => 'El campo nombre debe ser una cadena de texto',
            'name.min' => 'El campo nombre debe tener minimo 3 caracteres',
            'name.max' => 'El campo nombre debe tener maximo 250 caracteres'
        ]);
        $category = Category::findOrFail($id);
        $category->name = $data['name'];
        $category->slug = Str::slug($data['name']);
        $category->description = $request['description'];
        $category->parent_id = $request['parent_id'];
        $category->active = true;
        $image = $request->file('image');
        if($image) {
            $nameImage = time() . "_" . $image->getClientOriginalName();
            Storage::disk('public')->put('/images/categories/' . $nameImage, \File::get($image));
            $category->image = '/storage/images/categories/'.$nameImage;
        }
        $category->save();

        return redirect(route('categories.index'))->with('success', '¡La categoria se actualizo con exito!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();
        return redirect(route('categories.index'))->with('success', '¡La categoria se elimino con exito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        $category = Category::findOrFail($id);
        if($category->active){
            $category->active = false;
            $result = 'desactivo';
        } else {
            $category->active = true;
            $result = 'activo';
        }
        $category->save();
        return redirect(route('categories.index'))->with('success', 'La categoria se '. $result.' correctamente');
    }

}
