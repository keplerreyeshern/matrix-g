<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Gallery;
use App\Models\Image;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str as Str;

class NewsController extends Controller
{

    protected $pages;
    protected $title;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->title = "Noticias";
        $this->pages = 'news';

        View::share('title', $this->title);
        View::share('pages', $this->pages);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $newsAll = News::where('type', 'news')->get();
        return view('admin.news.list', compact('newsAll'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'title' => 'required',
            'intro' => 'required',
            'content' => 'required'
        ],[
            'title.required' => 'El campo titulo es obligatorio',
            'intro.required' => 'El campo introducción es obligatorio',
            'content.required' => 'El campo contenido es obligatorio',
        ]);
        $news = new News();
        $news->title = $data['title'];
        $news->slug = Str::slug($data['title']);
        $news->intro = $data['intro'];
        $news->content = $data['content'];
        $news->date = date_create('now');
        $news->type = 'news';
        $image = $request->file('image');
        if($image) {
            $nameImage = time() . "_" . $image->getClientOriginalName();
            Storage::disk('public')->put('/images/news/' . $nameImage, \File::get($image));
            $news->image = '/storage/images/news/'.$nameImage;
        }
        $news->save();
        $galleries = Gallery::whereNull('reference')->where('type', 'news')->get();
        if($galleries){
            for($i=0;count($galleries)>$i;$i++){
                $galleries[$i]->reference = $news->id;
                $galleries[$i]->save();
            }
        }

        return redirect(route('news.index'))->with('success', 'Se creo con exito la noticia');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::findOrFail($id);
        $gallery =  Gallery::where('active', true)->where('reference', $news->id)->where('type', 'news')->first();
        $images = [];
        if ($gallery){
            $images = Image::where('gallery_id', $gallery->id)->get();
        }
        return view('admin.news.edit', compact('news', 'images'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'title' => 'required',
            'intro' => 'required',
            'content' => 'required'
        ],[
            'title.required' => 'El campo titulo es obligatorio',
            'intro.required' => 'El campo introducción es obligatorio',
            'content.required' => 'El campo contenido es obligatorio',
        ]);
        $news = News::findOrFail($id);
        $news->title = $data['title'];
        $news->intro = $data['intro'];
        $news->content = $data['content'];
        $news->date = date_create('now');
        $image = $request->file('image');
        if($image) {
            $nameImage = time() . "_" . $image->getClientOriginalName();
            Storage::disk('public')->put('/images/news/' . $nameImage, \File::get($image));
            $news->image = '/storage/images/news/'.$nameImage;
        }
        $news->save();
        return redirect(route('news.index'))->with('success', 'Se actualizo con exito la noticia');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = News::findOrFail($id);
        $news->delete();
        return redirect(route('news.index'))->with('success', 'Se elimino con exito la noticia');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        $news = News::findOrFail($id);
        if($news->active){
            $news->active = false;
            $result = 'desactivo';
        } else {
            $news->active = true;
            $result = 'activo';
        }
        $news->save();
        return redirect(route('news.index'))->with('success', 'La noticia se '. $result.' correctamente');
    }
}
