<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Gallery;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str as Str;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = '';
        $type = $request['type'];
        $reference = $request['reference'];
        $gallery = Gallery::where('type', $type)->where('reference', $reference)->first();
        if ($gallery){
            $gallery_id = $gallery->id;
        } else {
            $galleryNew = new Gallery();
            $galleryNew->type = $type;
            $galleryNew->reference = $reference;
            $galleryNew->active = true;
            $galleryNew->save();
            $gallery_id = $galleryNew->id;
        }
        $image = new Image();
        $image->active = true;
        $image->gallery_id = $gallery_id;
        $file = $request->file('file');
        $nameFile = time() . "_" . $file->getClientOriginalName();
        Storage::disk('public')->put('/images/galleries/'.$type.'/' . $nameFile, \File::get($file));
        $image->name = '/storage/images/galleries/'.$type.'/'.$nameFile;
        $image->slug = Str::slug($nameFile);
        $image->save();

        return $image;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $image = Image::find($id);
        $image->delete();
        return \Redirect::back();
    }
}
