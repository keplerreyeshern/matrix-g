<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Gallery;
use App\Models\Image;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str as Str;

class ProductController extends Controller
{
    protected $pages;
    protected $title;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->title = "Productos";
        $this->pages = 'product';

        View::share('title', $this->title);
        View::share('pages', $this->pages);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::get();
        return view('admin.products.list', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::whereNull('parent_id')->get();
        return view('admin.products.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string|min:3|max:250'
        ],[
            'name.required' => 'El campo nombre es obligatorio',
            'name.string' => 'El campo nombre debe ser una cadena de texto',
            'name.min' => 'El campo nombre debe tener minimo 3 caracteres',
            'name.max' => 'El campo nombre debe tener maximo 250 caracteres'
        ]);
        $product = new Product();
        $product->name = $data['name'];
        $product->slug = Str::slug($data['name']);
        $product->details = $request['details'];
        $product->description = $request['description'];
        $product->model = $request['model'];
        $product->key = $request['key'];
        $image = $request->file('image');
        if($image) {
            $nameImage = time() . "_" . $image->getClientOriginalName();
            Storage::disk('public')->put('/images/products/' . $nameImage, \File::get($image));
            $product->image = '/storage/images/products/'.$nameImage;
        }
        $product->save();
        $product->categories()->sync($request['categories']);
        $galleries = Gallery::whereNull('reference')->where('type', 'products')->get();
        if($galleries){
            for($i=0;count($galleries)>$i;$i++){
                $galleries[$i]->reference = $product->id;
                $galleries[$i]->save();
            }
        }
        return redirect(route('products.index'))->with('success', 'El producto se creo correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);
        $categories = Category::whereNull('parent_id')->get();
        $gallery =  Gallery::where('active', true)->where('reference', $product->id)->where('type', 'products')->first();
        $images = [];
        if ($gallery){
            $images = Image::where('gallery_id', $gallery->id)->get();
        }
        return view('admin.products.edit', compact('product', 'categories', 'images'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'name' => 'required|string|min:3|max:250'
        ],[
            'name.required' => 'El campo nombre es obligatorio',
            'name.string' => 'El campo nombre debe ser una cadena de texto',
            'name.min' => 'El campo nombre debe tener minimo 3 caracteres',
            'name.max' => 'El campo nombre debe tener maximo 250 caracteres'
        ]);
        $product = Product::findOrFail($id);
        $product->name = $data['name'];
        $product->slug = Str::slug($data['name']);
        $product->details = $request['details'];
        $product->description = $request['description'];
        $product->model = $request['model'];
        $product->key = $request['key'];
        $image = $request->file('image');
        if($image) {
            $nameImage = time() . "_" . $image->getClientOriginalName();
            Storage::disk('public')->put('/images/products/' . $nameImage, \File::get($image));
            $product->image = '/storage/images/products/'.$nameImage;
        }
        $product->save();
        $product->categories()->sync($request['categories']);
        $galleries = Gallery::whereNull('reference')->where('type', 'products')->get();
        if($galleries){
            for($i=0;count($galleries)>$i;$i++){
                $galleries[$i]->reference = $product->id;
                $galleries[$i]->save();
            }
        }

        return redirect(route('products.index'))->with('success', 'El producto se actualizo correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();

        return redirect(route('products.index'))->with('success', 'El producto se elimino correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        $product = Product::findOrFail($id);
        if($product->active){
            $product->active = false;
            $result = 'desactivo';
        } else {
            $product->active = true;
            $result = 'activo';
        }
        $product->save();
        return redirect(route('products.index'))->with('success', 'El producto se '. $result.' correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function new($id)
    {
        $product = Product::findOrFail($id);
        if($product->new){
            $product->new = false;
            $result = 'viejo';
        } else {
            $product->new = true;
            $result = 'nuevo';
        }
        $product->save();
        return redirect(route('products.index'))->with('success', 'El producto se activo como '. $result.' correctamente');
    }
}
