<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Gallery;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str as Str;

class GalleryController extends Controller
{
    protected $pages;
    protected $title;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->title = "Galerias";
        $this->pages = 'gallery';

        View::share('title', $this->title);
        View::share('pages', $this->pages);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $galleries = Gallery::get();
        return view('admin.galleries.list', compact('galleries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.galleries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $gallery = Gallery::where('reference', 0)->where('type', 'general')->first();
        $data = $request->validate([
            'name' => 'required|string|min:3|max:250'
        ],[
            'name.required' => 'El campo nombre es obligatorio',
            'name.string' => 'El campo nombre debe ser una cadena de texto',
            'name.min' => 'El campo nombre debe tener minimo 3 caracteres',
            'name.max' => 'El campo nombre debe tener maximo 250 caracteres'
        ]);
        $gallery->name = $data['name'];
        $gallery->slug = Str::slug($data['name']);
        $gallery->save();

        return redirect(route('galleries.index'))->with('success', 'La galeria se creo con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $gallery = Gallery::findOrFail($id);
        return view('admin.galleries.show', compact('gallery'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gallery = Gallery::findOrFail($id);
        $images = Image::where('active', true)->where('gallery_id', $gallery->id)->get();
        return view('admin.galleries.edit', compact('gallery', 'images'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $gallery = Gallery::findOrFail($id);
        $data = $request->validate([
            'name' => 'required|string|min:3|max:250'
        ],[
            'name.required' => 'El campo nombre es obligatorio',
            'name.string' => 'El campo nombre debe ser una cadena de texto',
            'name.min' => 'El campo nombre debe tener minimo 3 caracteres',
            'name.max' => 'El campo nombre debe tener maximo 250 caracteres'
        ]);
        $gallery->name = $data['name'];
        $gallery->slug = Str::slug($data['name']);
        $gallery->save();

        return redirect(route('galleries.index'))->with('success', 'La galeria se creo con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gallery = Gallery::findOrFail($id);
        $gallery->delete();

        return redirect(route('galleries.index'))->with('success', 'La galeria se elimino correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        $gallery = Gallery::findOrFail($id);
        if($gallery->active){
            $gallery->active = false;
            $result = 'desactivo';
        } else {
            $gallery->active = true;
            $result = 'activo';
        }
        $gallery->save();
        return redirect(route('galleries.index'))->with('success', 'La galeria se '. $result.' correctamente');
    }
}
