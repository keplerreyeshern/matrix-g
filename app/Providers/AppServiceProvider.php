<?php

namespace App\Providers;

use App\Models\Category;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Carbon\Carbon::setLocale(config('app.locale'));
        Paginator::defaultView('vendor.pagination.bootstrap-4');
        Paginator::defaultSimpleView('vendor.pagination.simple-bootstrap-4');

        view()->composer('public.components.navbar', function ($view){
            $collection = Category::where('active', true)->whereNull('parent_id')->get();
            if(!$collection->isEmpty()){
                $categories = $collection;
            } else {
                $categories = [];
            }
            $view->with('categories', $categories);
        });

        view()->composer('public.components.footer', function ($view){
            $collection = Category::where('active', true)->whereNull('parent_id')->get();
            if(!$collection->isEmpty()){
                $categories = $collection;
            } else {
                $categories = [];
            }
            $view->with('categories', $categories);
        });
    }
}
