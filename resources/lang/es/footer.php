<?php

return [

    'about' => [
        'title' => 'Acerca de',
        'content' => 'Excess.com.mx CÓDIGO QUIERE SER SIMPLE es una iniciativa para ayudar a los futuros programadores con el código. Scanfcode se enfoca en proporcionar el código o fragmentos más eficientes ya que el código quiere ser simple. Ayudaremos a los programadores a construir conceptos en diferentes lenguajes de programación que incluyen C, C ++, Java, HTML, CSS, Bootstrap, JavaScript, PHP, Android, SQL y Algoritmo.'
    ],
    'categories' => 'categorias',
    'quick-links' => [
        'title' => 'enlaces rápidos',
        'link1' => 'Sobre Nosotros',
        'link2' => 'Contáctenos',
        'link3' => 'Contribuir',
        'link4' => 'Política de privacidad',
        'link5' => 'Administrador',
    ],
    'copryright' => 'Todos los Derechos Reservados por',

];
