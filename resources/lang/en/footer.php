<?php

return [

    'about' => [
        'title' => 'about',
        'content' => 'Excess.com.mx CODE WANTS TO BE SIMPLE is an initiative to help the upcoming programmers with the code. Scanfcode focuses on providing the most efficient code or snippets as the code wants to be simple. We will help programmers build up concepts in different programming languages that include C, C++, Java, HTML, CSS, Bootstrap, JavaScript, PHP, Android, SQL and Algorithm.'
    ],
    'categories' => 'categories',
    'quick-links' => [
        'title' => 'quick links',
        'link1' => 'About Us',
        'link2' => 'Contact Us',
        'link3' => 'Contribite',
        'link4' => 'Privacy Policy',
        'link5' => 'Administrator',
    ],
    'copryright' => 'All Rights Reserved by',

];
