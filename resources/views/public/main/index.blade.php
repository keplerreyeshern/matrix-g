@extends('public.layouts.app')

@section('title', '¡Bienvenido!')

@section('content')
    <div class="container-fluid">
        esta es la imagen de algo mas para poder tener contenido
        <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusamus at, consequuntur culpa cum eaque
            enim eos exercitationem impedit incidunt iusto magni modi nam, nemo perspiciatis quibusdam quo quos
            voluptates?
        </div>
        <div>Alias cum error, fuga laborum nobis nulla odio tenetur veritatis voluptatem voluptates! Aliquid at delectus
            deserunt, error est exercitationem facilis harum nisi nobis qui quod, quos reiciendis sit temporibus vel.
        </div>
        <div>A aspernatur autem eligendi excepturi exercitationem explicabo hic id laudantium maxime minima nam natus
            non nostrum obcaecati perspiciatis quasi ratione, repellendus totam ullam velit veniam voluptate voluptatum?
            Nihil, officiis, quae!
        </div>
        <div>Fuga laudantium molestiae quis sunt ullam. Beatae consequatur consequuntur cumque cupiditate eius enim esse
            exercitationem fuga harum hic in incidunt ipsam labore, molestiae neque quas qui similique, tempora velit
            veniam.
        </div>
        <div>Aspernatur dignissimos exercitationem fuga inventore maxime, mollitia neque, nihil nostrum nulla possimus
            quas quod sed voluptates. At atque dolores eveniet impedit nisi quae quas reiciendis rem sed, similique.
            Ipsam, officiis?
        </div>
        <div>Accusamus animi aperiam consequuntur cumque delectus deserunt dolore dolores dolorum eos esse eum eveniet
            id in iusto labore, minima neque nulla optio, qui quis ratione repudiandae sequi sunt voluptas voluptate.
        </div>
        <div>Beatae, dolorum, quia? Amet asperiores cumque, dolorum eaque eos et ex impedit inventore ipsum iusto
            maiores nemo nesciunt nobis odit omnis praesentium provident qui quia sunt suscipit totam, vel vitae.
        </div>
        <div>Dolorum ea hic provident, quaerat quas quis similique. Aliquid at cupiditate dicta dolorum itaque maiores
            maxime minima nulla! Autem earum nihil omnis praesentium quisquam reiciendis sunt. Perspiciatis quas vero
            voluptatibus!
        </div>
        <div>Ad at cum esse est fuga illo in ipsam maxime mollitia, nihil, provident rerum, sit sunt tempora
            voluptatibus! Ab adipisci asperiores autem inventore ipsa magnam neque quia temporibus, ullam ut.
        </div>
        <div>A atque, aut blanditiis culpa eligendi est hic illum iure laboriosam libero magnam nisi nulla obcaecati
            officiis praesentium reprehenderit rerum sequi tenetur ut velit! Eius minus mollitia pariatur quos.
            Pariatur.
        </div>
    </div>
@endsection
