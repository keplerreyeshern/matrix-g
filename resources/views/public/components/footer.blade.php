<!-- Site footer -->
<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <h6>@lang('footer.about.title')</h6>
                <p class="text-justify">@lang('footer.about.content')</p>
            </div>

            <div class="col-xs-6 col-md-3">
                <h6>Categories</h6>
                <ul class="footer-links">
                    @foreach($categories as $key => $category)
                        <li><a href="#">{{ $category->name }}</a></li>
                    @endforeach
                </ul>
            </div>

            <div class="col-xs-6 col-md-3">
                <h6>@lang('footer.quick-links.title')</h6>
                <ul class="footer-links">
                    <li><a href="http://scanfcode.com/about/">@lang('footer.quick-links.link1')</a></li>
                    <li><a href="http://scanfcode.com/contact/">@lang('footer.quick-links.link2')</a></li>
                    <li><a href="http://scanfcode.com/contribute-at-scanfcode/">@lang('footer.quick-links.link3')</a></li>
                    <li><a href="http://scanfcode.com/privacy-policy/">@lang('footer.quick-links.link4')</a></li>
                    <li><a href="/admin">@lang('footer.quick-links.link5')</a></li>
                </ul>
            </div>
        </div>
        <hr>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-6 col-xs-12">
                <p class="copyright-text">Copyright &copy; {{ date('Y') }} @lang('footer.copryright')
                    <a href="#">Excess</a>.
                </p>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <ul class="social-icons">
                    @if(App::isLocale('es'))
                        <li>
                            <a class="usa" href="{{ url('lang', ['en']) }}" title="Change Of Language">
                                <img src="{{ asset('images/flags/united.svg') }}" width="20px" class="img-fluid" alt="">
                            </a>
                        </li>
                    @elseif(App::isLocale('en'))
                        <li>
                            <a class="mexico" href="{{ url('lang', ['es']) }}" title="Cambio de Idioma">
                                <img src="{{ asset('images/flags/mexico.svg') }}" width="20px" class="img-fluid" alt="">
                            </a>
                        </li>
                    @endif
                    <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
{{--                    <li><a class="dribbble" href="#"><i class="fa fa-dribbble"></i></a></li>--}}
{{--                    <li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>--}}
                </ul>
            </div>
        </div>
    </div>
</footer>
