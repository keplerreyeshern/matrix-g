<header>
    <h1>
        <a href="{{ route('home') }}">
            <img src="{{ asset('images/logo.png') }}" class="img-fluid" alt="Logo">
        </a>
    </h1>

    <section class="menu">

        <input type="checkbox" id="menuCheck">
        <label for="menuCheck" class="menu-icon">
            <i class="fas fa-bars"></i>
            <i class="fas fa-times"></i>
        </label>

        <ul class="menu-list">
            <li class="nav-item">
                <a href="{{ route('home') }}" class="nav-link text-white">
                    @lang('navbar.home')
                </a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link text-white">
                    @lang('navbar.about')
                </a>
            </li>
            <li class="nav-item dropdown" >
                <a class="nav-link text-white dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @lang('navbar.categories')
                </a>
                <div class="dropdown-menu submenu" aria-labelledby="navbarDropdown">
                    @foreach($categories->sortby('name') as $key => $category)
                        <a class="nav-link text-white">{{ $category->name }}</a>
                    @endforeach
                </div>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link text-white">
                    @lang('navbar.contact')
                </a>
            </li>
        </ul>
    </section>
</header>
