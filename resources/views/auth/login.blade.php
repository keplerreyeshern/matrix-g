@extends('public.layouts.auth')

@section('title', '¡Entrar!')

@section('content')
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100 p-t-85 p-b-20">
                @if (session('error'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>{{ session('error') }}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div
                @endif
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="bg-login d-flex justify-content-center">
                        <img src="{{ asset('images/logo.png') }}" class="img-fluid w-25" alt="logo">
                    </div>
                    <div class="p-5">
                        <div class="row py-3">
                            <div class="col-12">
                                <input type="email" name="email" placeholder="Usuario" required class="input100 @error('email') is-invalid @enderror" >
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row py-3">
                            <div class="col-12">
                                <input type="password" name="password" placeholder="Contraseña" required class="input100 @error('password') is-invalid @enderror">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Recordarme') }}
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row py-1">
                            <div class="form-group">
                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('¿Olvidaste tu Contraseña?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                        <div class="row py-4 justify-content-center">
                            <button class="btn btn-login btn-lg" type="submit">Entrar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
