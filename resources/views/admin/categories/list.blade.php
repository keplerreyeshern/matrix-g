@extends('admin.layouts.app')

@section('title', 'Lista de Categorias')

@section('content')
    <div class="col-lg-12 px-md-5 bg-light mt-md-5">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="fa fa-warehouse"></i>
                    </div>
                    <div>
                        Categorias
                        <div class="page-title-subheading">Agrega, Modifica, Elimina.</div>
                    </div>
                </div>
                <div class="page-title-actions">
                    <a type="button"  data-toggle="tooltip" title="Agregar" href="{{ route('categories.create') }}" data-placement="bottom" class="btn-shadow mr-3 btn btn-primary">
                        <i class="fa fa-plus"></i>
                    </a>
                </div>
            </div>
        </div>
        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{ session('success') }}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div
        @endif
        <div class="row mt-3">
            <div class="col-12">
                <div class="main-card mb-3">
                    <div class=" table-responsive"><h5 class="card-title">Tabla de Categorias</h5>
                        @foreach($categories->sortby('name') as $key => $category)
                            <div class="row">
                                <div class="col-2">
                                    <a href="{{route('categories.edit', ['category' => $category->id])}}">
                                        @if($category->image)
                                            <img src="{{asset($category->image)}}" class="img-fluid" alt="{{$category->image}}" id="logo">
                                        @else
                                            <img src="{{asset('images/missing.png')}}" class="img-fluid" alt="Sin imagen" id="logo">
                                        @endif
                                    </a>
                                </div>
                                <div class="col-3">
                                    <a href="{{route('categories.edit', ['category' => $category->id])}}" class="nav-link text-dark">
                                        @if($category->name)
                                            {{ $category->name }}
                                        @else
                                            Editar
                                        @endif
                                    </a>
                                </div>
                                <div class="col-7">
                                    <div class="row justify-content-end">
                                        <a type="submit" class="btn" href="{{ route('categories.createChild', ['parent_id' => $category->id]) }}">
                                            <i class="fa fa-plus text-primary"></i>
                                        </a>
                                        <form method="POST" action="{{ route('categories.active', ['id' => $category->id]) }}">
                                            @csrf
                                            <button type="submit" class="btn">
                                                @if($category->active)
                                                    <i class="fa fa-power-off text-success"></i>
                                                @else
                                                    <i class="fa fa-power-off text-danger"></i>
                                                @endif
                                            </button>
                                        </form>
                                        <form method="POST" action="{{ route('categories.destroy', ['category' => $category->id]) }}">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn text-danger">
                                                <i class="fa fa-trash-alt"></i>
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <children-categories-component :parent_id="{{$category->id}}" :language="'{{ config('app.locale') }}'"></children-categories-component>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
