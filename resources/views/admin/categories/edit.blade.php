@extends('admin.layouts.app')

@section('title', isset($parent_id) == true ? 'Editar SubCategoria de la Categoria'. $categoryParent->name:'Editar Categoria')

@section('content')
    <div class="col-lg-12 px-md-5 bg-light mt-md-5">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="fa fa-warehouse"></i>
                    </div>
                    <div>
                        Editar Categoria
                        <div class="page-title-subheading">Editar categoria.</div>
                    </div>
                </div>
                <div class="page-title-actions">
                    <a type="button" href="{{ route('categories.index') }}" data-toggle="tooltip" title="Lista" data-placement="bottom" class="btn-shadow mr-3 btn btn-primary">
                        <i class="fa fa-list"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="main-card mb-3 ">
                    <div class="">
                        <form class="validate-form mb-5" method="POST" action="{{route('categories.update', ['category' => $category->id])}}" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            @if(isset($parent_id))
                                <h5 class="card-title"><span class="text-info">Sub Categoria de la Categoria </span> {{ $categoryParent->name }}</h5>
                                <input type="hidden" name="parent_id" value="{{ $parent_id }}">
                            @endif
                            <div class="form-row">
                                <div class="col-12">
                                    <div class="position-relative form-group">
                                        <label for="name">
                                            @if(App::islocale('es'))
                                                <img src="{{ asset('images/flags/Flag_of_mexico.png') }}" class="img-fluid" width="20px" alt="Spanish">
                                            @endif
                                            @if(App::islocale('en'))
                                                <img src="{{ asset('images/flags/Flag_of_United.svg') }}" class="img-fluid" width="20px" alt="English">
                                            @endif
                                            Nombre
                                        </label>
                                        <input name="name" id="name" placeholder="Nombre" type="text" class="form-control" required value="{{ old('name', $category->name) }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-12">
                                    <label for="">
                                        @if(App::islocale('es'))
                                            <img src="{{ asset('images/flags/Flag_of_mexico.png') }}" class="img-fluid" width="20px" alt="Spanish">
                                        @endif
                                        @if(App::islocale('en'))
                                            <img src="{{ asset('images/flags/Flag_of_United.svg') }}" class="img-fluid" width="20px" alt="English">
                                        @endif
                                        Descripción
                                    </label>
                                    <textarea class="ckeditor mt-3 @error('description') is-invalid @enderror bg-info" placeholder="Descripción"
                                              name="description" value="{{old('description', $category->description)}}">{{old('description', $category->description)}}</textarea>
                                    @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mt-5">
                                <div class="col-4">
                                    <label class="text-center">Imagen</label>
                                    <br>
                                    @if($category->image)
                                        <img src="{{asset($category->image)}}" class="img-fluid" alt="{{ $category->image }}" id="image">
                                    @else
                                        <img src="{{asset('images/missing.png')}}" class="img-fluid" alt="Sin imagen" id="image">
                                    @endif
                                </div>
                                <div class="col-8">
                                    <br><br>
                                    <div class="alert alert-primary" role="alert">
                                        <i class="fa fa-file-image"></i>
                                        Image (Formato png, jpg o jpeg 320 x 640 resolución 72 dpi max 900kb)<br>
                                    </div>

                                    <div class="upload-btn-wrapper" id="btnimage">
                                        <button class="btn btn-primary">Selecciona un archivo</button>
                                        <input type="file" name="image" accept="image/*" onchange="loadFileImage(event)">
                                    </div>
                                    <div id="deleteimage">
                                        <a class="btn btn-danger" onclick="deleteImage()">Eliminar Imagen</a>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-12">
                                    <div class="alert alert-info col-12" role="alert">
                                        <i class="fa fa-images"></i>
                                        Fotos (formato png, jpg o jpeg 1024 x 768 resolución 72 dpi )
                                    </div>
                                    <dropzone-component :reference="'{{ $category->id }}'" type="categories"></dropzone-component>
                                </div>
                            </div>
                            <div class="row justify-content-end">
                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        <i class="fa fa-edit"></i>
                                        Actualizar
                                    </button>
                                </div>
                            </div>
                        </form>
                        <div class="row mb-5">
                            @if (session('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <strong>{{ session('success') }}</strong>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                            @foreach($images as $image)
                                <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                                    <img src="{{asset($image->name)}}" class="img-fluid" alt="{{$image->name}}" id="logo">
                                    <form method="POST" action="{{ route('images.destroy', ['image' => $image->id]) }}">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn text-danger">
                                            <i class="fa fa-trash-alt"></i>
                                        </button>
                                    </form>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if($category->image)
            <script type="text/javascript"> var imageExist = true;</script>
        @else
            <script type="text/javascript"> var imageExist = false;</script>
        @endif
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        if (imageExist){
            document.getElementById('btnimage').style.display = 'none';
            document.getElementById('deleteimage').style.display = 'block';
        } else {
            document.getElementById('btnimage').style.display = 'block';
            document.getElementById('deleteimage').style.display = 'none';
        }

        function deleteImage(){
            document.getElementById('deleteimage').style.display = 'none';
            document.getElementById('btnimage').style.display = 'block';
            var image = document.getElementById('image');
            image.src = '/images/missing.png';
            image.value = "";
        }

        var loadFileImage = function(event) {
            var reader = new FileReader();
            reader.onload = function(){
                var image = document.getElementById('image');
                image.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
            document.getElementById('btnimage').style.display = 'none';
            document.getElementById('deleteimage').style.display = 'block';
        };
    </script>
@endsection
