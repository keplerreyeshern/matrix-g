@extends('admin.layouts.app')

@if($pages == 'news')
    @section('title', 'Lista de Noticias')
@elseif($pages == 'blog')
    @section('title', 'Lista de Blog')
@endif

@section('content')
    <div class="col-lg-12 px-md-5 bg-light mt-md-5">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="fa fa-store"></i>
                    </div>
                    <div>
                        @if($pages == 'news')
                            Noticias
                        @elseif($pages == 'blog')
                            Blog
                        @endif
                        <div class="page-title-subheading">Agrega, Modifica, Elimina.</div>
                    </div>
                </div>
                <div class="page-title-actions">
                    @if($pages == 'news')
                        <a type="button"  data-toggle="tooltip" title="Agregar" href="{{ route('news.create') }}" data-placement="bottom" class="btn-shadow mr-3 btn btn-primary">
                            <i class="fa fa-plus"></i>
                        </a>
                    @elseif($pages == 'blog')
                        <a type="button"  data-toggle="tooltip" title="Agregar" href="{{ route('blog.create') }}" data-placement="bottom" class="btn-shadow mr-3 btn btn-primary">
                            <i class="fa fa-plus"></i>
                        </a>
                    @endif

                </div>
            </div>
        </div>
        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{ session('success') }}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div
        @endif
        <div class="row mt-3">
            <div class="col-12">
                <div class="main-card mb-3 ">
                    <div class="table-responsive">
                        <h5 class="card-title">Tabla de
                            @if($pages == 'news')
                                Noticias
                            @elseif($pages == 'blog')
                                Blog
                            @endif
                        </h5>
                        <table class="mb-0 table table-hover mb-3">
                            <thead>
                            <tr>
                                <th>Imagen</th>
                                <th>Nombre</th>
                                <th>Introduccion</th>
                                <th>Fecha</th>
                                <th class="text-center" colspan="2">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($newsAll->sortby('date') as $key => $news)
                                <tr>
                                    <td width="200px">
                                        @if($pages == 'news')
                                            <a href="{{route('news.edit', ['news' => $news->id])}}">
                                                @if($news->image)
                                                    <img src="{{asset($news->image)}}" class="img-fluid" alt="{{$news->image}}" id="poster">
                                                @else
                                                    <img src="{{asset('images/missing.png')}}" class="img-fluid" alt="Sin imagen" id="poster">
                                                @endif
                                            </a>
                                        @elseif($pages == 'blog')
                                            <a href="{{route('blog.edit', ['blog' => $news->id])}}">
                                                @if($news->image)
                                                    <img src="{{asset($news->image)}}" class="img-fluid" alt="{{$news->image}}" id="poster">
                                                @else
                                                    <img src="{{asset('images/missing.png')}}" class="img-fluid" alt="Sin imagen" id="poster">
                                                @endif
                                            </a>
                                        @endif
                                    </td>
                                    <td>
                                        @if($pages == 'news')
                                            <a href="{{route('news.edit', ['news' => $news->id])}}">
                                                @if($news->title)
                                                    {{$news->title}}
                                                @else
                                                    Editar
                                                @endif
                                            </a>
                                        @elseif($pages == 'blog')
                                            <a href="{{route('blog.edit', ['blog' => $news->id])}}">
                                                @if($news->title)
                                                    {{$news->title}}
                                                @else
                                                    Editar
                                                @endif
                                            </a>
                                        @endif
                                    </td>
                                    <td>{!! $news->intro !!}</td>
                                    <td>{{ \Carbon\Carbon::parse($news->date)->translatedFormat('l d F Y') }}</td>
                                    <td width="10px">
                                        @if($pages == 'news')
                                            <form method="POST" action="{{ route('news.active', ['news' => $news->id]) }}">
                                                @csrf
                                                <button type="submit" class="btn">
                                                    @if($news->active)
                                                        <i class="fa fa-power-off text-success"></i>
                                                    @else
                                                        <i class="fa fa-power-off text-danger"></i>
                                                    @endif
                                                </button>
                                            </form>
                                        @elseif($pages == 'blog')
                                            <form method="POST" action="{{ route('blog.active', ['blog' => $news->id]) }}">
                                                @csrf
                                                <button type="submit" class="btn">
                                                    @if($news->active)
                                                        <i class="fa fa-power-off text-success"></i>
                                                    @else
                                                        <i class="fa fa-power-off text-danger"></i>
                                                    @endif
                                                </button>
                                            </form>
                                        @endif
                                    </td>
                                    <td width="10px">
                                        @if($pages == 'news')
                                            <form method="POST" action="{{ route('news.destroy', ['news' => $news->id]) }}">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn text-danger">
                                                    <i class="fa fa-trash-alt"></i>
                                                </button>
                                            </form>
                                        @elseif($pages == 'blog')
                                            <form method="POST" action="{{ route('blog.destroy', ['blog' => $news->id]) }}">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn text-danger">
                                                    <i class="fa fa-trash-alt"></i>
                                                </button>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
