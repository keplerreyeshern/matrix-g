@extends('admin.layouts.app')

@if($pages == 'news')
    @section('title', 'Editar Noticia')
@elseif($pages == 'blog')
    @section('title', 'Editar Blog')
@endif

@section('content')
    <div class="col-lg-12 px-md-5 bg-light mt-md-5">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="fa fa-warehouse"></i>
                    </div>
                    <div>
                        @if($pages == 'news')
                            Editar Noticia
                        @elseif($pages == 'blog')
                            Editar Blog
                        @endif
                        <div class="page-title-subheading">
                            @if($pages == 'news')
                                Modificar Noticia
                            @elseif($pages == 'blog')
                                Modificar Blog
                            @endif
                        </div>
                    </div>
                </div>
                <div class="page-title-actions">
                    @if($pages == 'news')
                        <a type="button" href="{{ route('news.index') }}" data-toggle="tooltip" title="Lista" data-placement="bottom" class="btn-shadow mr-3 btn btn-primary">
                            <i class="fa fa-list"></i>
                        </a>
                    @elseif($pages == 'blog')
                        <a type="button" href="{{ route('blog.index') }}" data-toggle="tooltip" title="Lista" data-placement="bottom" class="btn-shadow mr-3 btn btn-primary">
                            <i class="fa fa-list"></i>
                        </a>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="main-card mb-3 ">
                    <div class="">
                        @if($pages == 'news')
                            <form class="validate-form mb-5" method="POST" action="{{ route('news.update', ['news'=> $news->id]) }}" enctype="multipart/form-data">
                        @elseif($pages == 'blog')
                            <form class="validate-form mb-5" method="POST" action="{{ route('blog.update', ['blog'=> $news->id]) }}" enctype="multipart/form-data">
                        @endif
                                @method('PUT')
                                @csrf
                                <div class="row mt-5">
                                    <div class="col-12">
                                        <label for="">
                                            @if(App::islocale('es'))
                                                <img src="{{ asset('images/flags/Flag_of_mexico.png') }}" class="img-fluid" width="20px" alt="Spanish">
                                            @endif
                                            @if(App::islocale('en'))
                                                <img src="{{ asset('images/flags/Flag_of_United.svg') }}" class="img-fluid" width="20px" alt="English">
                                            @endif
                                            Titulo
                                        </label>
                                        <input type="text" class="form-control mt-3 @error('title') is-invalid @enderror" placeholder="Titulo" name="title" value="{{old('title', $news->title)}}">
                                        @error('title')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row mt-5">
                                    <div class="col-12">
                                        <label for="">
                                            @if(App::islocale('es'))
                                                <img src="{{ asset('images/flags/Flag_of_mexico.png') }}" class="img-fluid" width="20px" alt="Spanish">
                                            @endif
                                            @if(App::islocale('en'))
                                                <img src="{{ asset('images/flags/Flag_of_United.svg') }}" class="img-fluid" width="20px" alt="English">
                                            @endif
                                            Introducción
                                        </label>
                                        <textarea class="ckeditor mt-3 @error('intro') is-invalid @enderror" placeholder="Introducción"
                                                  name="intro" value="{{old('intro', $news->intro)}}">{{old('intro', $news->intro)}}</textarea>
                                        @error('intro')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row mt-5">
                                    <div class="col-12">
                                        <label for="">
                                            @if(App::islocale('es'))
                                                <img src="{{ asset('images/flags/Flag_of_mexico.png') }}" class="img-fluid" width="20px" alt="Spanish">
                                            @endif
                                            @if(App::islocale('en'))
                                                <img src="{{ asset('images/flags/Flag_of_United.svg') }}" class="img-fluid" width="20px" alt="English">
                                            @endif
                                            Contenido
                                        </label>
                                        <textarea class="ckeditor mt-3 @error('content') is-invalid @enderror" placeholder="Contenido"
                                                  name="content" value="{{old('content', $news->content)}}">{{old('content', $news->content)}}</textarea>
                                        @error('content')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row mt-5">
                                    <div class="col-4">
                                        <label class="text-center">Imagen</label>
                                        <br>
                                        <img src="{{asset('images/missing.png')}}" class="img-fluid" alt="Sin imagen" id="image">
                                    </div>
                                    <div class="col-8">
                                        <br><br>
                                        <div class="alert alert-primary" role="alert">
                                            <i class="fa fa-file-image"></i>
                                            Imagen (Formato png, jpg o jpeg 320 x 640 resolución 72 dpi max 900kb)<br>
                                        </div>

                                        <div class="upload-btn-wrapper" id="btnimage">
                                            <button class="btn btn-primary">Selecciona un archivo</button>
                                            <input type="file" name="image" accept="image/*" onchange="loadFileImage(event)">
                                        </div>
                                        <div id="deleteimage">
                                            <a class="btn btn-danger" onclick="deleteImage()">Eliminar Imagen</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-12">
                                        <div class="alert alert-info col-12" role="alert">
                                            <i class="fa fa-images"></i>
                                            Fotos (formato png, jpg o jpeg 1024 x 768 resolución 72 dpi )
                                        </div>
                                        @if($pages == 'news')
                                            <dropzone-component :reference="{{ $news->id }}" type="news"></dropzone-component>
                                        @elseif($pages == 'blog')
                                            <dropzone-component :reference="{{ $news->id }}" type="blog"></dropzone-component>
                                        @endif

                                    </div>
                                </div>
                            <div class="row justify-content-end mt-3">
                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        <i class="fa fa-save"></i>
                                        Actualizar
                                    </button>
                                </div>
                            </div>
                        </form>
                       @foreach($images as $image)
                           <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                               <img src="{{asset($image->name)}}" class="img-fluid" alt="{{$image->name}}" id="logo">
                               <form method="POST" action="{{ route('images.destroy', ['image' => $image->id]) }}">
                                   @csrf
                                   @method('DELETE')
                                   <button type="submit" class="btn text-danger">
                                       <i class="fa fa-trash-alt"></i>
                                   </button>
                               </form>
                           </div>
                       @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        document.getElementById('deleteimage').style.display = 'none';
        function deleteImage(){
            document.getElementById('deleteimage').style.display = 'none';
            document.getElementById('btnimage').style.display = 'block';
            var image = document.getElementById('image');
            image.src = '/images/missing.png';
            image.value = "";
        }

        var loadFileImage = function(event) {
            var reader = new FileReader();
            reader.onload = function(){
                var image = document.getElementById('image');
                image.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
            document.getElementById('btnimage').style.display = 'none';
            document.getElementById('deleteimage').style.display = 'block';
        };
    </script>
@endsection
