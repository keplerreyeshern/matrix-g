<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    @yield('head')
    @yield('styles')
</head>
<body>
<div id="app">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 col-lg-2">
                @include('admin.components.sidebar')
            </div>
            <main class="col-md-9 col-lg-10">
                <div class="row">
                    <div class="col-12">
                        @include('admin.components.navbar')
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 bg-light mt-md-5">
                        @yield('content')
                        @include('admin.components.footer')
                    </div>
                </div>
            </main>
        </div>
    </div>
</div>
<!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/libs/jquery-1.6.2.min.js"><\/script>')</script>
<script>
    $(window).load(function (){
        var btnopen = $("#btnopen");
        var btnclose = $("#btnclose")
        var sidebar = $("#sidebarMenu");
        var open = false;
        btnopen.click(function (){
            if (!open){
                open = true;
                sidebar.addClass('frontSidebar');
                sidebar.show('slow');
            }
        });
        btnclose.click(function (){
            if(open) {
                open = false;
                sidebar.removeClass('frontSidebar');
                sidebar.hide('slow')
            }
        });
    });
</script>
@yield('scripts')
</body>
</html>
