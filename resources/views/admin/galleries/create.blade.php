@extends('admin.layouts.app')

@section('title', 'Nueva Galeria')

@section('content')
    <div class="col-lg-12 px-md-5 bg-light mt-md-5">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="fa fa-warehouse"></i>
                    </div>
                    <div>
                        Nueva Galeria
                        <div class="page-title-subheading">Agregar galeria.</div>
                    </div>
                </div>
                <div class="page-title-actions">
                    <a type="button" href="{{ route('galleries.index') }}" data-toggle="tooltip" title="Lista" data-placement="bottom" class="btn-shadow mr-3 btn btn-primary">
                        <i class="fa fa-list"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="main-card mb-3">
                    <div class="">
                        <form class="validate-form mb-5" method="POST" action="{{route('galleries.store')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-row">
                                        <div class="col-12">
                                            <div class="position-relative form-group">
                                                <label for="name">
                                                    @if(App::islocale('es'))
                                                        <img src="{{ asset('images/flags/Flag_of_mexico.png') }}" class="img-fluid" width="20px" alt="Spanish">
                                                    @endif
                                                    @if(App::islocale('en'))
                                                        <img src="{{ asset('images/flags/Flag_of_United.svg') }}" class="img-fluid" width="20px" alt="English">
                                                    @endif
                                                    Nombre
                                                </label>
                                                <input name="name" id="name" placeholder="Nombre" type="text" class="form-control" required value="{{ old('name') }}">
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-12">
                                    <div class="alert alert-info col-12" role="alert">
                                        <i class="fa fa-images"></i>
                                        Fotos (formato png, jpg o jpeg 1024 x 768 resolución 72 dpi )
                                    </div>
                                    <dropzone-component :reference="'0'" type="general"></dropzone-component>
                                </div>
                            </div>
                            <div class="row justify-content-end">
                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        <i class="fa fa-save"></i>
                                        Guardar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
