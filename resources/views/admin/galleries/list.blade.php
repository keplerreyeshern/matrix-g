@extends('admin.layouts.app')

@section('title', 'Lista de Galerias')

@section('content')
    <div class="col-lg-12 px-md-5 bg-light mt-md-5">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="fa fa-images"></i>
                    </div>
                    <div>
                        Galerias
                        <div class="page-title-subheading">Agrega, Modifica, Elimina.</div>
                    </div>
                </div>
                <div class="page-title-actions">
                    <a type="button"  data-toggle="tooltip" title="Agregar" href="{{ route('galleries.create') }}" data-placement="bottom" class="btn-shadow mr-3 btn btn-primary">
                        <i class="fa fa-plus"></i>
                    </a>
                </div>
            </div>
        </div>
        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{ session('success') }}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div
        @endif
        <div class="row mt-3">
            <div class="col-12">
                <div class="main-card mb-3">
                    <div class= table-responsive"><h5 class="card-title">Tabla de Galerias</h5>
                        <table class="mb-0 table table-hover mb-3">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>Nombre</th>
                                <th>Tipo</th>
                                <th class="text-center" colspan="2">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($galleries->sortby('name') as $key => $gallery)
                                <tr>
                                    <td>
                                        {{ $key + 1 }}
                                    </td>
                                    <td>
                                        <a href="{{ route('galleries.edit', ['gallery' => $gallery->id]) }}" class="nav-link">
                                            @if($gallery->name)
                                                {{ $gallery->name }}
                                            @else
                                                Editar
                                            @endif
                                        </a>
                                    </td>
                                    <td>
                                        @switch($gallery->type)
                                            @case('categories')
                                                {{ __('Categorias') }}
                                            @break
                                            @case('products')
                                                {{ __('Productos') }}
                                            @break
                                            @case('blog')
                                                {{ __('Blog') }}
                                            @break
                                            @case('news')
                                                {{ __('Noticias') }}
                                            @break
                                            @default
                                                {{ __('Generico') }}
                                            @break
                                        @endswitch
                                    </td>
                                    <td width="50px">
                                        <form method="POST" action="{{ route('galleries.active', ['gallery' => $gallery->id]) }}">
                                            @csrf
                                            <button type="submit" class="btn">
                                                @if($gallery->active)
                                                    <i class="fa fa-power-off text-success"></i>
                                                @else
                                                    <i class="fa fa-power-off text-danger"></i>
                                                @endif
                                            </button>
                                        </form>
                                    </td>
                                    <td width="50px">
                                        <form method="POST" action="{{ route('galleries.destroy', ['gallery' => $gallery->id]) }}">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn text-danger">
                                                <i class="fa fa-trash-alt"></i>
                                            </button>
                                        </form></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
