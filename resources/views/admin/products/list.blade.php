@extends('admin.layouts.app')

@section('title', 'Lista de Productos')

@section('content')
    <div class="col-lg-12 px-md-5 bg-light mt-md-5">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="fa fa-store"></i>
                    </div>
                    <div>
                        Productos
                        <div class="page-title-subheading">Agrega, Modifica, Elimina.</div>
                    </div>
                </div>
                <div class="page-title-actions">
                    <a type="button"  data-toggle="tooltip" title="Agregar" href="{{ route('products.create') }}" data-placement="bottom" class="btn-shadow mr-3 btn btn-primary">
                        <i class="fa fa-plus"></i>
                    </a>
                </div>
            </div>
        </div>
        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{ session('success') }}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div
        @endif
        <div class="row mt-3">
            <div class="col-12">
                <div class="main-card mb-3">
                    <div class=" table-responsive"><h5 class="card-title">Tabla de Productos</h5>
                        <table class="table table-hover mb-3">
                            <thead>
                            <tr>
                                <th>Imagen</th>
                                <th>Nombre</th>
                                <th>Modelo</th>
                                <th class="text-center" colspan="3">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($products->sortby('name') as $key => $product)
                                <tr>
                                    <td width="200px">
                                        <a href="{{ route('products.edit', ['product' => $product->id]) }}">
                                            @if($product->image)
                                                <img src="{{asset($product->image)}}" class="img-fluid" alt="{{$product->image}}" id="image">
                                            @else
                                                <img src="{{asset('images/missing.png')}}" class="img-fluid" alt="Sin imagen" id="image">
                                            @endif
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ route('products.edit', ['product' => $product->id]) }}" class="nav-link text-dark">
                                            @if($product->name)
                                                {{ $product->name }}
                                            @else
                                                Editar
                                            @endif
                                        </a>
                                    </td>
                                    <td>
                                        {{ $product->model }}
                                    </td>
                                    <td width="10px">
                                        <form method="POST" action="{{ route('products.active', ['product' => $product->id]) }}">
                                            @csrf
                                            <button type="submit" class="btn">
                                                @if($product->active)
                                                    <i class="fa fa-power-off text-success"></i>
                                                @else
                                                    <i class="fa fa-power-off text-danger"></i>
                                                @endif
                                            </button>
                                        </form>
                                    </td>
                                    <td width="10px">
                                        <form method="POST" action="{{ route('products.new', ['product' => $product->id]) }}">
                                            @csrf
                                            <button type="submit" class="btn text-info">
                                                @if($product->new)
                                                    <i class="fas fa-star"></i>
                                                @else
                                                    <i class="far fa-star"></i>
                                                @endif
                                            </button>
                                        </form>
                                    </td>
                                    <td width="10px">
                                        <form method="POST" action="{{ route('products.destroy', ['product' => $product->id]) }}">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn text-danger">
                                                <i class="fa fa-trash-alt"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
