<nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block sidebar collapse">
    <div class="row justify-content-between">
        <div>
            <a class="col-md-3 col-lg-2" href="{{route('dashboard')}}">
                <img src="{{ asset('images/logo.png') }}" class="img-fluid m-1" alt="Logo">
            </a>
        </div>
        <div class="pr-3">
            <button class="btn d-md-none ml-auto text-white " id="btnclose">
                <i class="fa fa-times"></i>
            </button>
        </div>
    </div>
    <div class="position-sticky pt-1">
        <ul class="nav flex-column">
            <li class="nav-item ">
                <span class="nav-link"><i class="fa fa-language"></i>
                    Estas Administrando en
                    @if (App::isLocale('en'))
                        Ingles
                    @elseif (App::isLocale('es'))
                        Español
                    @endif
                </span>
                @if (App::isLocale('en'))
                    <a class="nav-link text-white" href="{{ url('lang', ['es']) }}">
                        <span>Administra en</span>
                        <img src="{{ asset('images/flags/Flag_of_mexico.png') }}" width="20px" alt="spanish" style="cursor: pointer;">
                    </a>
                @elseif (App::isLocale('es'))
                    <a class="nav-link text-white" href="{{ url('lang', ['en']) }}">
                        <span>Administra en</span>
                        <img src="{{ asset('images/flags/Flag_of_United.svg') }}" width="20px" alt="english" style="cursor: pointer;">
                    </a>
                @endif
            </li>
            <li class="nav-item pt-3">
                <a class="nav-link {{ $pages == 'dashboard' ? 'active':'' }}" aria-current="page" href="{{ route('dashboard')}}">
                    <i class="fa fa-home"></i>
                    Home
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ $pages == 'user' ? 'active':'' }}" aria-current="page" href="{{ route('users.index')}}">
                    <i class="fa fa-users"></i>
                    Usuarios
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ $pages == 'category' ? 'active':'' }}" aria-current="page" href="{{ route('categories.index')}}">
                    <i class="fa fa-warehouse"></i>
                    Categorias
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ $pages == 'product' ? 'active':'' }}" aria-current="page" href="{{ route('products.index')}}">
                    <i class="fa fa-store-alt"></i>
                    Productos
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ $pages == 'gallery' ? 'active':'' }}" href="{{ route('galleries.index')}}">
                    <i class="fa fa-images"></i>
                    Galerias
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ $pages == 'news' ? 'active':'' }}" href="{{ route('news.index')}}">
                    <i class="fa fa-newspaper"></i>
                    Noticias
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ $pages == 'blog' ? 'active':'' }}" href="{{ route('blog.index')}}">
                    <i class="fa fa-blog"></i>
                    Blog
                </a>
            </li>

        </ul>
    </div>
</nav>
