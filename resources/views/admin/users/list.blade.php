@extends('admin.layouts.app')

@section('title', 'Lista de Usuarios')

@section('content')
    <div class="col-lg-12 px-md-5 bg-light mt-md-5">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="fa fa-store"></i>
                    </div>
                    <div>
                        Usuarios
                        <div class="page-title-subheading">Agrega, Modifica, Elimina.</div>
                    </div>
                </div>
                <div class="page-title-actions">
                    <a type="button"  data-toggle="tooltip" title="Agregar" href="{{ route('users.create') }}" data-placement="bottom" class="btn-shadow mr-3 btn btn-primary">
                        <i class="fa fa-plus"></i>
                    </a>
                </div>
            </div>
        </div>
        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{ session('success') }}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div
        @endif
        <div class="row mt-3">
            <div class="col-12">
                <div class="main-card mb-3">
                    <div class=" table-responsive"><h5 class="card-title">Tabla de Usuarios</h5>
                        <table class="mb-0 table table-hover mb-3">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>Nombre</th>
                                <th>Correo</th>
                                <th class="text-center" colspan="2">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users->sortby('name') as $key => $user)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>
                                        <a href="{{ route('users.edit', ['user' => $user->id]) }}">
                                            {{ $user->name }}
                                        </a>
                                    </td>
                                    <td>{{ $user->email }}</td>
                                    <td width="10px">
                                        <form method="POST" action="{{ route('users.active', ['user' => $user->id]) }}">
                                            @csrf
                                            <button type="submit" class="btn">
                                                @if($user->active)
                                                    <i class="fa fa-power-off text-success"></i>
                                                @else
                                                    <i class="fa fa-power-off text-danger"></i>
                                                @endif
                                            </button>
                                        </form>
                                    </td>
                                    <td width="10px">
                                        <form method="POST" action="{{ route('users.destroy', ['user' => $user->id]) }}">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn text-danger">
                                                <i class="fa fa-trash-alt"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
