@extends('admin.layouts.app')

@section('title', 'Editar Usuario')

@section('content')
    <div class="col-lg-12 px-md-5 bg-light mt-md-5">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="fa fa-warehouse"></i>
                    </div>
                    <div>
                        Editar Usuario
                        <div class="page-title-subheading">Modificar usuario.</div>
                    </div>
                </div>
                <div class="page-title-actions">
                    <a type="button" href="{{ route('users.index') }}" data-toggle="tooltip" title="Lista" data-placement="bottom" class="btn-shadow mr-3 btn btn-primary">
                        <i class="fa fa-list"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="main-card mb-3">
                    <div class="">
                        <form class="validate-form mb-5" method="POST" action="{{ route('users.update', ['user' => $user->id]) }}">
                            @csrf
                            @method('PUT')
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-row">
                                        <div class="col-12">
                                            <div class="position-relative form-group">
                                                <label for="name">Nombre</label>
                                                <input name="name" id="name" placeholder="Nombre" type="text" class="form-control @error('name') is-invalid @enderror" required value="{{ old('name', $user->name) }}">
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-12">
                                            <div class="position-relative form-group">
                                                <label for="email">Correo</label>
                                                <input name="email" id="email" placeholder="Nombre" type="text" class="form-control @error('email') is-invalid @enderror" required value="{{ old('email', $user->email) }}">
                                                @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-12">
                                            <label for="password">Contraseña</label>
                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-12">
                                            <label for="password-confirm">Confirma la Contraseña</label>
                                            <input name="password_confirmation" id="password-confirm" placeholder="Confirma la Contraseña" type="password" class="form-control" required value="{{ old('password_confirmation') }}">
                                            @error('password-confirm')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-end mt-3">
                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        <i class="fa fa-save"></i>
                                        Guardar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
