/**
 * @license Copyright (c) 2003-2020, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
    config.cloudServices_uploadUrl = 'https://33333.cke-cs.com/easyimage/upload/',
        // Note: this is a token endpoint to be used for CKEditor 4 samples only. Images uploaded using this token may be deleted automatically at any moment.
        // To create your own token URL please visit https://ckeditor.com/ckeditor-cloud-services/.
    config.cloudServices_tokenUrl = 'https://33333.cke-cs.com/token/dev/ijrDsqFix838Gh3wGO3F77FSW94BwcLXprJ4APSp3XQ26xsUHTi0jcb1hoBt',
    config.easyimage_styles = {
        gradient1: {
            group: 'easyimage-gradients',
                attributes: {
                'class': 'easyimage-gradient-1'
            },
            label: 'Blue Gradient',
                icon: 'https://ckeditor.com/docs/ckeditor4/4.15.1/examples/assets/easyimage/icons/gradient1.png',
                iconHiDpi: 'https://ckeditor.com/docs/ckeditor4/4.15.1/examples/assets/easyimage/icons/hidpi/gradient1.png'
        },
        gradient2: {
            group: 'easyimage-gradients',
                attributes: {
                'class': 'easyimage-gradient-2'
            },
            label: 'Pink Gradient',
                icon: 'https://ckeditor.com/docs/ckeditor4/4.15.1/examples/assets/easyimage/icons/gradient2.png',
                iconHiDpi: 'https://ckeditor.com/docs/ckeditor4/4.15.1/examples/assets/easyimage/icons/hidpi/gradient2.png'
        },
        noGradient: {
            group: 'easyimage-gradients',
                attributes: {
                'class': 'easyimage-no-gradient'
            },
            label: 'No Gradient',
                icon: 'https://ckeditor.com/docs/ckeditor4/4.15.1/examples/assets/easyimage/icons/nogradient.png',
                iconHiDpi: 'https://ckeditor.com/docs/ckeditor4/4.15.1/examples/assets/easyimage/icons/hidpi/nogradient.png'
        }
    }
};
