<?php

use App\Http\Controllers\Admin\BlogController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\GalleryController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\ImageController;
use App\Http\Controllers\Admin\NewsController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Client\MainController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('storage-link', function (){
    \Illuminate\Support\Facades\Artisan::call('storage:link');
});*/

Auth::routes();


Route::group(['middleware' => ['lang']], function () {
    Route::get('lang/{lang}', function ($lang) {
        session(['lang' => $lang]);
        return \Redirect::back();
    })->where([
        'lang' => 'en|es'
    ]);

    Route::get('', [MainController::class,'index'])->name('home');
});

Route::group(['prefix' => 'admin','middleware' => ['auth', 'lang']], function () {

    /*  Rutas de Administración */
    Route::get('', [HomeController::class, 'index'])->name('dashboard');
    Route::resource('/users', UserController::class)->except(['show']);
    Route::post('/users/active/{user}', [UserController::class, 'active'])->name('users.active');
    Route::resource('/categories', CategoryController::class);
    Route::post('/categories/active/{id}', [CategoryController::class, 'active'])->name('categories.active');
    Route::get('/categories/create/child/{parent_id}', [CategoryController::class, 'createChild'])->name('categories.createChild');
    Route::resource('/products', ProductController::class);
    Route::post('/products/active/{product}', [ProductController::class, 'active'])->name('products.active');
    Route::post('/products/new/{product}', [ProductController::class, 'new'])->name('products.new');
    Route::resource('/galleries', GalleryController::class);
    Route::post('/galleries/active/{gallery}', [GalleryController::class, 'active'])->name('galleries.active');
    Route::resource('/news', NewsController::class);
    Route::post('/news/active/{news}', [NewsController::class, 'active'])->name('news.active');
    Route::post('/blog/active/{blog}', [BlogController::class, 'active'])->name('blog.active');
    Route::resource('/blog', BlogController::class);
    Route::resource('/images', ImageController::class)->only(['store', 'destroy']);
});
